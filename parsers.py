#!/usr/bin/env python2.7

import re
import codecs
import xml.etree.ElementTree as ET

from Structures import Question, PrintStmt

class XMLParser(object):
    def __init__(self, fn):
        self.tree = ET.parse(fn)
        self.root = self.tree.getroot()
        self.questions = []
        self.abbreviations = {}

        # Load abbreviations
        for abbr in self.root.iter('abbr'):
            if self.abbreviations.get(abbr.attrib["eq"]) is None:
                self.abbreviations[abbr.attrib["eq"]] = abbr.text
            else:
                s = "Abbreviation {} is already defined.".format(abbr.attrib["eq"])
                raise Exception(s)

        i = 1
        for elem in self.root:

            # Parse questions
            if elem.tag == "q":
                q = elem
                # The answer
                a = q.find('a')

                q_text = self.repl(q.text)
                a_text = self.repl(a.text)

                self.questions.append(Question(q_text, a_text, i))
                i += 1

            # Parse PrintStmt
            if elem.tag == "verb":
                self.questions.append(
                    PrintStmt(unicode(ET.tostring(elem), "UTF-8"))
                )

    def repl(self, s):
        pattern = r"\${}"
        for abbr in self.abbreviations:
            s = re.sub(pattern.format(abbr), self.abbreviations[abbr], s)
        return s

    def get_questions(self):
        return self.questions
        
def main():
    p = XMLParser('samples/sample.xml')
    questions = p.get_questions()
    print questions

if __name__ == "__main__":
    main()
