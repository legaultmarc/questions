import re

class Question(object):
    def __init__(self, q, r, i):
        self.index = i
        self.question = unicode(q)
        self.ans = unicode(r)
    def __repr__(self):
        self.question = re.sub(r"\s+", " ", self.question)
        self.ans = re.sub(r"\s+", " ", self.ans)
        return "'{} --> {}'".format(self.question, self.ans)

class PrintStmt(object):
    def __init__(self, html):
        self.html = unicode(html)

class IncludeStmt(object):
    def __init__(self, html):
        self.html = unicode(html)
