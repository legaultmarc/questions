#!/usr/bin/env python2.7

import argparse
import re
import codecs

import parsers
from Structures import Question, PrintStmt, IncludeStmt

qi = {
        'author': "Marc-Andre Legault",
        'email': "marc-andre.legault.1@umontreal.ca",
        'version': 2.0
     }

try:
    from jinja2 import Environment, PackageLoader
except ImportError:
    print "Install jinja2:\n\tpip install jinja2\nto use QuestIons"

def handle_xml_input(args):


    # Parse the xml
    p = parsers.XMLParser(args.qdb_file)
    questions = p.get_questions()

    # Count the questions
    i = 0
    for j in questions:
        if type(j) is Question:
            i += 1

    qi["total"] = i
    rendered_html = render(args, {
        "questions": questions,
        "qi": qi,
        "mathjax": args.mathjax,
        "stylesheet": args.stylesheet,
    })

    if args.output_file:
        with codecs.open(args.output_file, 'wb', encoding="UTF-8") as output_file:
            output_file.write(rendered_html)
    else:
        print rendered_html

def render(args, info):
    try:
        env = Environment(loader=PackageLoader('QuestIons', 'templates'))
    except ImportError as e:
        print "Install setuptools"
        raise e

    template = env.get_template(args.template)
    return template.render(**info)

def main(args):
    if args.format == "xml" or args.qdb_file.endswith('.xml'):
        return handle_xml_input(args)

    questions = []
    unrecognized = []

    # Parse of the form: Question? \? Answer.
    pattern = re.compile(r"^(.*)\s\\\?\s(.*)\n$")
    question_counter = 0
    line_counter = 0
    with codecs.open(args.qdb_file, 'rb', encoding="UTF-8") as input_file:
        for line in input_file:

            line_counter += 1
            # Parse line
            matcher = pattern.match(line)
            if matcher:
                question = matcher.group(1)
                ans = matcher.group(2)
                question_counter += 1
                questions.append(Question(question, ans, question_counter))
            else:
                # Ignore empty lines
                if re.match(r"^\s+$", line):
                    pass
                # Ignore comments
                elif line.startswith("#"):
                    # Unless they are print satetements, then
                    # treat print statements like questions, the
                    # template will need to notice the difference.
                    if line.startswith("# print"):
                        questions.append(PrintStmt(line.split(" print ")[1]))

                    # Support for include statements: read a file and
                    # pass it's raw contents to the template.
                    elif line.startswith("# include"):
                        filename = line.split("include")[1]
                        filename = re.sub(r"\s+", "", filename)
                        include_data = []
                        with codecs.open(filename, 'rb', encoding="utf-8") as include_file:
                            for line in include_file:
                                include_data.append(line)
                        questions.append(IncludeStmt("".join(include_data)))
                            
                else:
                    line = re.sub(r'\n', '', line)
                    print u"Unrecognized statement at line {}:\n\t{}".format(line_counter, line)
                    unrecognized.append(line)

    qi["total"] = question_counter

    rendered_html = render(args, {
        "questions": questions, 
        "qi": qi, 
        "mathjax": args.mathjax,
        "stylesheet": args.stylesheet
    })

    if args.output_file:
        with codecs.open(args.output_file, 'wb', encoding="utf-8") as output_file:
            output_file.write(rendered_html)
    else:
        print rendered_html

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
                                        description = ("Interprete un fichier "
                                                       "de base de donnee QuestIons.")
                                    )
    parser.add_argument(
                        "qdb_file", 
                        type=str, 
                        help=("The QuestIons qdb file containing the questions "
                              "to be compiled."), 
                        metavar="qdb file"
                       )

    parser.add_argument(
                        "--template", 
                        type=str, 
                        help="Jinja2 template file to render questions.",
                        metavar="template",
                        dest="template",
                        required=False,
                        default="default.html",
                       )

    parser.add_argument(
                        "--out", 
                        type=str, 
                        help="Output html file.",
                        metavar="out",
                        dest="output_file",
                        required=False,
                       )

    parser.add_argument(
                        "--mathjax",
                        help="Enable MathJax",
                        dest="mathjax",
                        required=False,
                        action='store_true',
                       )

    parser.add_argument(
                        "--stylesheet",
                        help="Relative path to css stylesheet",
                        dest="stylesheet",
                        required=False
                       )

    parser.add_argument(
                        "--format",
                        choices=("qdb", "xml"),
                        default="qdb",
                        help=("The input format for the question database. "
                              "default is qdb format, but XML is allowed since "
                              "version 3")
                        )

    args = parser.parse_args()

    main(args)
