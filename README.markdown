# QuestIons #
This software was created as a study help. While reviewing for an exam, I would prepare questions in a text file (_qdb_ file). The basic syntax is as follows:
    
    # A Comment
    # print <p>Some html for the top of the generated html file</p>
    A questions \? An answer!

The generated quiz can then be shared easily (_e.g._ hosted on a web server) where your fellow classmates can take the quiz and/or improve it.

I personally had a good general experience with this method. It helps a lot when you need to know a lot of facts by heart. Hope it helps others!

Also note that it is preferable to use unix style UTF-8 encoding. The `samples/` directory contains an example (you could even copy-paste this file to start your _qdb_ files).

## Dependencies ##

+ Python2.7
+ Jinja2
+ setuptools

## Sample usage ##

    $ ./QuestIons.py my_questions_database.qdb --out html_quiz.html

